import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import axios from 'axios';



export default class App extends React.Component {

  componentWillMount = () => this.fetchCategories();

  fetchCategories = () => {
    axios.get('localhost:3000/')
  .then(function (response) {
    console.log(response);
})
  .catch(function (error) {
    console.log(error);
});
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
